const {City} = require('./City')

class Country{

    constructor(name, cities = []) {
        this.name = name
        this.cities = cities
    }

    async addCity(name){
        await this.cities.push(new City(name))
    }

    removeCity(name) {
        const removeIndex = this.cities.findIndex(city => city.name === name)
        this.cities.splice(removeIndex, 2)
    }

    async setWeatherForCities() {
        for (const city of this.cities) {
            await city.setWeather()
        }
    }

    showWeather() {
        this.cities.forEach( city => {
            console.log(`${city.name}: `, city.weather)
        });
    }

    sortByTemp () {
        this.cities.sort( (a,b) => this.getTemperature(b) - this.getTemperature(a))
    }

    getTemperature (city) {
        const { temperature } = city.weather
        return +temperature.slice(0,temperature.indexOf(" "))
        console.log(+temperature.slice(0, temperature.indexOf(" ")))
    }

}

module.exports = {Country}