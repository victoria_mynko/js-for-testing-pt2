const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'


class Weather{

    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = Date.now()
        this.forecast = []
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => {return error.message})
    }

    async setForecast(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast = response.data.forecast;
        }).catch((error) => {return error.message});
    }


    async setWeatherAndForecast(cityName){
        await this.setWeather(cityName);
        await this.setForecast(cityName);
    }
}

module.exports = {Weather}