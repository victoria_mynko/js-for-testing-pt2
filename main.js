const {City} = require('./City')
const {Country} = require('./Country');

const newCities = ['Odesa', 'Zaporizhzhia', 'Mykolaiv', 'Vinnytsia', 'Kherson', 'Poltava', 'Chernivtsi', 'Sumy']

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')


const showResult = async () =>  {

    let theCountry = new Country('Ukraine', [city1, city2])

    newCities.forEach( cityName => theCountry.addCity(cityName))

    await theCountry.setWeatherForCities()

    theCountry.removeCity('Kherson')
    theCountry.sortByTemp()
    theCountry.showWeather()
};

showResult()